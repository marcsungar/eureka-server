package com.Hotel.EurekaManager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class EurekaManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(EurekaManagerApplication.class, args);
	}

}
